## Safe Deployments with AWS Lambda

AWS Lambda and AWS CodeDeploy recently made it possible to automatically shift incoming traffic between two function versions based on a preconfigured rollout strategy. This new feature allows you to gradually shift traffic to the new function. If there are any issues with the new code, you can quickly rollback and control the impact to your application.

The sample code here supports the [Safe Deployments with AWS Lambda](https://aws.amazon.com/blogs/compute/implementing-safe-aws-lambda-deployments-with-aws-codedeploy/) Blog post. 

https://github.com/aws/serverless-application-model/blob/master/docs/safe_lambda_deployments.rst


## Build, Run the example

 - In file deploy.sh, edit and update 
   ```export AWS_PROFILE=``` 
 - Execute ```./deploy.sh``` from root project directory (This will create the first version of the lambda)
 - Update returnS3Buckets.zip in template.yml to returnS3BucketsNew.zip [see here](https://bitbucket.org/aashwin-gaur-pwc/example-linear-deployment/src/4a2b44a10e6548b7d02f397f41c8ecfcb55f18c6/template.yaml#lines-13)
 - Re-run ```./deploy.sh``` 
 
This will create a new version of the lambda and deploy it with the strategy as per the 'DeploymentPreferences.Type' in the template.yaml [see here](https://bitbucket.org/aashwin-gaur-pwc/example-linear-deployment/src/4a2b44a10e6548b7d02f397f41c8ecfcb55f18c6/template.yaml#lines-24)
 
## Tips

This repo is configured with Deployment Strategy 'Linear10PercentEvery1Minute' but it can be changed to various other values as per [this list](https://github.com/awslabs/serverless-application-model/blob/master/docs/safe_lambda_deployments.rst#traffic-shifting-configurations)