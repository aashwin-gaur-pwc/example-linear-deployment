#set the aws profile before running this
export AWS_PROFILE=

ACCOUNT_ID=$(aws sts get-caller-identity --query Account --output text)
echo "Working in account - $ACCOUNT_ID"

REGION=ap-southeast-2

######### Create deployment bucket ######################################################################
S3_BUCKET_NAME=$ACCOUNT_ID-deployment-resources-bucket
echo "Deployment bucket - $S3_BUCKET_NAME"
BUCKET_EXISTS=$(aws s3api head-bucket --bucket $S3_BUCKET_NAME 2>&1 || true)
if ! [ -z "$BUCKET_EXISTS" ]; then
    aws s3 mb s3://$S3_BUCKET_NAME \
    --region $REGION
fi
##############################################################################


########### Build zips and upload to s3 #############################################################################
mkdir build

zip build/returnS3BucketsNew.zip returnS3BucketsNew.js
zip build/returnS3Buckets.zip returnS3Buckets.js
zip build/preTrafficHook.zip preTrafficHook.js 

aws s3 cp \
    build/returnS3BucketsNew.zip \
    s3://$S3_BUCKET_NAME/

aws s3 cp \
    build/returnS3Buckets.zip \
    s3://$S3_BUCKET_NAME/

aws s3 cp \
    build/preTrafficHook.zip \
    s3://$S3_BUCKET_NAME/

########################################################################################


########### Deploy template #############################################################################

sam package \
    --template-file template.yaml \
    --output-template-file build/deploy.yaml \
     --s3-bucket $S3_BUCKET_NAME \
     --region $REGION 

sam deploy \
    --template-file build/deploy.yaml \
    --stack-name test-deploy-linear \
    --region $REGION \
    --capabilities CAPABILITY_IAM

##########################################################################